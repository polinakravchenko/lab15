﻿// lab15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Cents
{
private:
    int m_cents{};

public:
    Cents(int cents)
        : m_cents{ cents }
    {}

    int getCents() const { return m_cents; }
};

Cents operator+(const Cents& c1, const Cents& c2)
{
    return Cents{ c1.getCents() + c2.getCents() };
}

int main()
{
    Cents cents1{ 10 };
    Cents cents2{ 8 };
    Cents centsSum{ cents1 + cents2 };
    std::cout << "I have " << centsSum.getCents() << " cents.\n";

    return 0;
}