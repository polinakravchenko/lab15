﻿// lab15_1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Cents
{
private:

    double m_x{};
    double m_y{};
    double m_z{};

public:

    Cents(double x = 0.0, double y = 0.0, double z = 0.0)
        : m_x{ x }, m_y{ y }, m_z{ z }
    {
    }

    friend std::ostream& operator<< (std::ostream& out, const Cents& cents);
};

std::ostream& operator<< (std::ostream& out, const Cents& cents)
{
    out << "Cents(" << cents.m_x << ", " << cents.m_y << ", " << cents.m_z << ')';

    return out;
}

int main()
{
 Cents Cents1{ 2.0, 3.5, 4.0 };
 Cents Cents2{ 6.0, 7.5, 8.0 };

    std::cout << Cents1 << ' ' << '\n';
    std::cout << Cents2 << ' ' << '\n';
    return 0;
}